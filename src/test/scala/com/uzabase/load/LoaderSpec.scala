package com.uzabase.load

import com.uzabase.load.Loader.{ResourceLoader, WebLoader}
import org.scalatest.{FlatSpec, Matchers}
import com.uzabase.load.LoaderType._

class LoaderSpec extends FlatSpec with Matchers {

  it should "return the WebLoader" in {
    val loader = Loader.apply(Web)

    loader.isInstanceOf[WebLoader] shouldBe true
  }

  it should "return the ResourceLoader" in {
    val loader = Loader.apply(Resource)

    loader.isInstanceOf[ResourceLoader] shouldBe true
  }

  it should "return the rss from resource" in {
    val loader = Loader.apply(Resource)

    val rss = loader.load("some.rss")

    rss shouldBe "<rss>\nsome rss\n</rss>"
  }

  it should "throw the exception and write to std err" in {
    val loader = Loader.apply(Resource)

    assertThrows[java.lang.RuntimeException] {
      loader.load("doesnt.exist")
    }
  }

}
