package com.uzabase

import org.scalatest.{FlatSpec, Matchers}

class MainSpec extends FlatSpec with Matchers {

  it should "throw exception because of zero arguments" in {
    assertThrows[java.lang.RuntimeException] {
      Main.main(Array.empty)
    }
  }
}
