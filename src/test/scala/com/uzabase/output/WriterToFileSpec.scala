package com.uzabase.output

import com.uzabase.Config
import com.uzabase.output.TestUtils._
import org.scalatest.{FlatSpec, Matchers}

import scala.reflect.io.Path

class WriterToFileSpec extends FlatSpec with Matchers {

  private val mockOutput = List("the first output text", "the second output text")
  private val outputPath = Config.appConfig.outputPath

  it should "write outputs into the files" in {
    val writer = new WriterToFile()
    writer.write(mockOutput)

    for (i <- mockOutput.indices) {
      val result = loadFile(s"$outputPath/rss-file-$i")
      mockOutput(i) shouldEqual result
    }

    deleteFiles(outputPath)
  }

  it should "print nothing for empty input" in {
    val writer = new WriterToFile()
    writer.write(List())

    val dir = Path(s"$outputPath")
    dir.exists shouldBe true

    for (i <- mockOutput.indices) {
      val file = Path(s"$outputPath/rss-file-$i")
      file.exists shouldBe false
    }

    deleteFiles(outputPath)
  }
}
