package com.uzabase.output

import org.scalatest.{FlatSpec, Matchers}

class WriterToStdOutSpec extends FlatSpec with Matchers {

  private val mockOutput = List("the first output text", "the second output text")

  it should "print all outputs into std out" in {
    val stream = new java.io.ByteArrayOutputStream()

    Console.withOut(stream) {

      val writer = new WriterToStdOut()
      writer.write(mockOutput)
    }

    stream.toString shouldBe s"${mockOutput(0)}\n${mockOutput(1)}\n"
  }

  it should "print nothing for empty input" in {
    val stream = new java.io.ByteArrayOutputStream()

    Console.withOut(stream) {

      val writer = new WriterToStdOut()
      writer.write(List())
    }

    stream.toString shouldBe ""
  }
}
