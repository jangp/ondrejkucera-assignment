package com.uzabase.output

import scala.io.Source
import scala.reflect.io.Path

object TestUtils {

  def deleteFiles(outputPath: String) {
    val path = Path(outputPath)
    path.deleteRecursively()
  }

  def loadFile(path: String): String = {
    val file = Source.fromFile(path)
    file.mkString
  }


}
