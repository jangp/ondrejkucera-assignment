package com.uzabase.output

import com.uzabase.Config
import com.uzabase.output.TestUtils._
import org.scalatest.{FlatSpec, Matchers}

import scala.reflect.io.Path

class PrinterSpec extends FlatSpec with Matchers {

  private val mockOutput = List("the first output text", "the second output text")
  private val outputPath = Config.appConfig.outputPath


  it should "write outputs into the files and sdt out" in {
    val stream = new java.io.ByteArrayOutputStream()
    Console.withOut(stream) {
      Printer.write(mockOutput)
    }

    stream.toString shouldBe s"${mockOutput(0)}\n${mockOutput(1)}\n"

    for (i <- mockOutput.indices) {
      val result = loadFile(s"$outputPath/rss-file-$i")
      mockOutput(i) shouldEqual result
    }

    deleteFiles(outputPath)
  }

  it should "write nothing into the files and sdt out (empty input)" in {
    val stream = new java.io.ByteArrayOutputStream()
    Console.withOut(stream) {
      Printer.write(List())
    }

    stream.toString shouldBe ""

    val dir = Path(s"$outputPath")
    dir.exists shouldBe true

    for (i <- mockOutput.indices) {
      val file = Path(s"$outputPath/rss-file-$i")
      file.exists shouldBe false
    }

    deleteFiles(outputPath)
  }

}
