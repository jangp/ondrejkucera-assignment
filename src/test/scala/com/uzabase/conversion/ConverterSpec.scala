package com.uzabase.conversion

import org.scalatest.{FlatSpec, Matchers}

class ConverterSpec extends FlatSpec with Matchers {

  val mock_rss = "This is really long text."

  it should "use the successful operation" in {
    val op = new OperationRemoveWord()

    val result = Converter.run(op, mock_rss)

    result shouldBe "This is really  text."
  }

  it should "throw exception for null of operation" in {
    assertThrows[IllegalArgumentException] {
      Converter.run(null, mock_rss)
    }
  }

  it should "throw exception for null of string parameter" in {
    val op = new OperationRemoveWord()

    assertThrows[IllegalArgumentException] {
      Converter.run(op, null)
    }
  }

}
