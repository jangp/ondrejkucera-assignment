package com.uzabase.conversion

import org.scalatest.{FlatSpec, Matchers}

class OperationRemoveWordSpec extends FlatSpec with Matchers {

  val mock_rss = "This is really long text."

  it should "remove word 'long' from text" in {
    val operation = new OperationRemoveWord()

    val result = operation.execute(mock_rss)
    result shouldBe "This is really  text."
  }
}
