package com.uzabase.output

class WriterToStdOut extends Writer {
  override def write(rssList: List[String]): Unit = {
    rssList.foreach(println)
  }
}
