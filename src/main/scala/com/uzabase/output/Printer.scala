package com.uzabase.output

object Printer {

  private val writerQueue = Array(
    new WriterToStdOut,
    new WriterToFile
  )

  def write(rssList: List[String]): Unit = {
    writerQueue.foreach(writer => {
      writer.write(rssList)
    })
  }
}
