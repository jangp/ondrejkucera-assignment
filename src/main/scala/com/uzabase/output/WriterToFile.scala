package com.uzabase.output

import java.io.{File, PrintWriter}

import com.uzabase.Config

class WriterToFile extends Writer {

  private val path = Config.appConfig.outputPath

  override def write(rssList: List[String]): Unit = {
    createDirs(path)
    for (i <- rssList.indices) {
      val file = new File(s"$path/rss-file-$i")
      createFile(file, rssList(i))
    }
  }

  def createDirs(path: String): Unit = {
    val dir = new File(s"$path")
    dir.mkdirs()
  }

  def createFile(file: File, content: String): Unit = {
    val writer = new PrintWriter(file)
    writer.write(content)
    writer.close()
  }
}
