package com.uzabase

import java.io.File

import com.uzabase.conversion.{Converter, OperationRemoveWord}
import com.uzabase.load.Loader
import com.uzabase.load.LoaderType._
import com.uzabase.output.Printer

object Main extends App {
  checkArguments(args)
  checkDirectory()

  val loader = Loader(Web)
  val rssList = args.toList.map(url => loader.load(url))

  val removeFun = new OperationRemoveWord()
  val resultList = rssList.map(rss => Converter.run(removeFun, rss))

  Printer.write(resultList)

  def checkDirectory(): Unit = {
    val path = Config.appConfig.outputPath
    if (new File(path).exists()) {
      val errorMessage = "The App can not be run. The Output directory already " +
        "exist and can contain previous output"
      System.err.println(s"ERROR Main - $errorMessage")
      throw new RuntimeException(errorMessage)
    }
  }

  def checkArguments(args: Array[String]): Unit = {
    if (args.length < 1) {
      val errorMessage = "More than one input argument has to be specified."
      System.err.println(s"ERROR Main - $errorMessage")
      throw new RuntimeException(errorMessage)
    }
  }
}