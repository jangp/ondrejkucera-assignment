package com.uzabase.load

import com.uzabase.load.LoaderType._

import scala.io.{Codec, Source}

trait Loader {
  def load(path: String): String
}

object Loader {

  implicit val codec: Codec = Codec("UTF-8")

  class WebLoader extends Loader {
    override def load(url: String): String = {
      try {
        val file = Source.fromURL(url)
        file.mkString
      }
      catch {
        case e: Throwable => {
          val errorMessage = "Problem with loading rss from Web"
          System.err.println(s"ERROR WebLoader - $errorMessage")
          throw new RuntimeException(errorMessage, e)
        }
      }
    }
  }

  class ResourceLoader extends Loader {
    override def load(path: String): String = {
      try {
        val file = Source.fromResource(path)
        file.mkString
      }
      catch {
        case e: Throwable => {
          val errorMessage = "Problem with loading rss from Resource"
          System.err.println(s"ERROR WebLoader - $errorMessage")
          throw new RuntimeException(errorMessage, e)
        }
      }
    }
  }

  // the factory method
  def apply(loader: LoaderType): Loader = {
    loader match {
      case Web => new WebLoader()
      case Resource => new ResourceLoader()
    }
  }
}


