package com.uzabase.load

object LoaderType extends Enumeration {
  type LoaderType = Value
  val Web, Resource = Value
}
