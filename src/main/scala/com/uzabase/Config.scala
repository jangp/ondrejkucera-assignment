package com.uzabase

import com.typesafe.config.ConfigFactory

case class Config(excludeWord: String, outputPath: String)

object Config {

  private val config = ConfigFactory.load()

  val appConfig = Config(
    config.getString("app.exclude-word"),
    config.getString("app.output-path")
  )
}
