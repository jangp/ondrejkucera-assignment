package com.uzabase.conversion

trait Operation {
  def execute(originalRss: String): String
}
