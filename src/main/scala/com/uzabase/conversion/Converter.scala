package com.uzabase.conversion

object Converter {

  def run(op: Operation, rss: String): String = {
    if (op == null) {
      val errorMessage = "the operation is not defined"
      System.err.println(s"ERROR Converter - $errorMessage")
      throw new IllegalArgumentException(errorMessage)
    }

    if (rss == null) {
      val errorMessage = "the operation is not defined"
      System.err.println(s"ERROR Converter - $errorMessage")
      throw new IllegalArgumentException(errorMessage)
    }

    op.execute(rss)
  }

}
