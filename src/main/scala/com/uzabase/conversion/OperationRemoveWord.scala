package com.uzabase.conversion

import com.uzabase.Config

class OperationRemoveWord extends Operation {

  override def execute(originalRss: String): String = {
    val word = Config.appConfig.excludeWord
    originalRss.replaceAll(word, "")
  }
}
