name := "rss-feed"

version := "0.1"

scalaVersion := "2.12.3"

libraryDependencies ++= Seq(
  "com.typesafe"               % "config" % "1.3.2",

  "org.scalactic"              %% "scalactic" % "3.0.5",
  "org.scalatest"              %% "scalatest" % "3.0.5" % "test"
)

scoverage.ScoverageKeys.coverageMinimum := 80
scoverage.ScoverageKeys.coverageFailOnMinimum := false