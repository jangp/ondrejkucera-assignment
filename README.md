# Rss Feed

## Run or Test on local environment
#### build the project
 - `sbt assembly` and you can find the jar in `target/scala-2.12/` directory

#### run
 - run in local: `scala target/scala-2.12/rss-feed-assembly-0.1.jar http://tech.uzabase.com/rss`
 - run in local with more rss feeds: `scala target/scala-2.12/rss-feed-assembly-0.1.jar http://tech.uzabase.com/rss http://tech.uzabase.com/rss`

#### test
  - run tests locally: `sbt coverage test coverageReport`
 
## Run or Test in Docker
#### test app in docker
 - build docker image `docker build -t rss-feed .`
 - run tests in docker `docker-compose up test` (note: first run take a while because of sbt)
#### run app in docker
 - build the project `sbt assembly`
 - build the docker image with jar `docker build -t rss-feed .`
 - run the app in docker container `docker-compose up prod`